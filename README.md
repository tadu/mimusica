# miMusica

![](https://i.imgur.com/5elmLfi.png)

Si te apestan itunes, spotify y otras moderneces; si quieres escuchar tus caciones de siempre que bajaste una vez
de Audiogalxy prueba **miMusica**: el reproductor de audio sin gilipolleces.


![](https://i.imgur.com/T8y4l1k.png)

## Cómo funciona

miMusica es un script de python (3.5 o superior) que se correo contra una carpeta en la que debemos tener nuestros mp3 ordenados
(preferentemente genero/autor/álbum).

![](https://i.imgur.com/GtQQVVd.png)


El script genera en cada carpeta un fichero html (index.html) que debe ser servido por un servidor web (apache, ngnix u otro)
local o remoto.

Si en la carpeta hay una imagen jpg la usará como imagen de la página. Si hay varias usará la última

	http://{ip o dominio}/musica

Demo: 

	http://musica.david.atauri.es

## Limitaciones de la versión actual

* Solo para LINUX / OSX / WIN. (NOOO fuenrual en android ni ios)
* Los nombres de fichero y carpeta no pueden tener comillas simples
* Los audios no se reproducen en móviles o tablets
* Las imágenes deben tener extensión jpg
* Los audios deben tener extensión mp3 o mp4
* la carpeta principal debe llamarse **musica** (ni music ni Musica ni música...)

## How to

* Copiar los audios e imágenes a la carpeta html o htdocs del servidor web
* copiar la carpeta player a la carpeta musica
* Abrir en una consola la carpeta player

> python3 noConazo.py

* Done!

## Cositas que arreglar

* Que muestre relojtos mientras carga, deditos en los enlaces y ese tipo de detalles
* copias locales de todas las librerías (que pueda correr en localhost sin internete)