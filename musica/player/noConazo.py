import os
import glob
import socket

extensionesAudio=['.mp3', '.mp4', '.MP3', '.MP4']
extensionesImg=['.jpg', '.JPG', '.jpeg', '.png', '.PNG']

def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


def crearIndex(root, dir):

    carpetas=[]
    mp3 = []
    breadCrumb=[]

    # contruir el path completo
    x=root+"/"+dir+"/*"

    # recuperar ficheros y carpetas
    x = glob.glob(x, recursive=False)
    x.sort()

    for filename in x:

        f = filename[ filename.rindex('/')+1:] # solo el nombre del tema
        extension = ""
        try:
            extension = f[f.rindex('.'):]
            if (extension in extensionesAudio):

                mp3.append({'audio': f, 'playing': False})

        except:
            pt = (root + "/" + dir + "/"+f)
            print("** ", f, " dir?", os.path.isdir(pt) )
            if os.path.isdir(pt):
                 carpetas.append(f)


    # get /musica..../carpeta para la url
    path = root
    try: path = root[root.index('musica'):]
    except: pass

    print(mp3)
    print(carpetas)

    # get import ipdb; ipdb.set_trace()
    ip = get_ip_address()
    pt = "http://"+ip+"/" # NO!!! get la ip
    migas = path.split('/')
    for c in migas:
        pt += c+"/"
        breadCrumb.append({'text': c, 'href':pt })
    # anadir la carpeta actual
    breadCrumb.append({'text': dir, 'href':".." })
    urlfoto=""
    # la foto (jpg)

    fotos = glob.iglob(root+"/"+dir+"/*.jpg", recursive=False)

    for foto in fotos:
        print("\nFOTO:"+foto)
        urlfoto = foto[ foto.rindex('/')+1:]
        print("\nFOTO:" + urlfoto)

    # abrir la plantilla
    f = open("index.html", 'r')
    allFile = f.read()
    #print(allFile)

    temas = "false"
    if len(mp3)>0:
        temas = str(mp3)


    # print(temas)
    allFile = allFile.replace("##IP", "http://"+ip)
    allFile = allFile.replace("##CARPETAS", str(carpetas))
    allFile = allFile.replace("##CURRPATH", myDir)
    allFile = allFile.replace("##TEMAS", temas)
    allFile = allFile.replace("False", "false")
    allFile = allFile.replace("##TITULO", dir)
    allFile = allFile.replace("##PATH", path+'/'+dir+'/')
    allFile = allFile.replace("##BREAD", str(breadCrumb))

    allFile = allFile.replace("##FOTO", urlfoto)

    #print(allFile)


    # guardar el index
    if (len(mp3)>0 or len(carpetas)>0):

        index = root+'/'+dir+'/index.html'
        # print("\n"+index)
        f = open( index, 'w')
        f.write(allFile)
        f.close()



def recorrer(path):

    for root, dirs, files in os.walk(path, topdown=False):
        for d in dirs:
            print("dir: ", root,  d, files)
            if not d=='player': crearIndex(root, d)

myDir = os.path.dirname(os.path.abspath(__file__))
musicDir = os.path.abspath(os.path.join(myDir, os.pardir)) # sube un nivel
print(musicDir)
crearIndex(musicDir, '.')
recorrer(musicDir)
